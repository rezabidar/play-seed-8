name := """play-scala-seed"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies += guice
libraryDependencies += ws
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.shikbook.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.shikbook.binders._"

// https://mvnrepository.com/artifact/org.reactivemongo/play2-reactivemongo
libraryDependencies += "org.reactivemongo" %% "play2-reactivemongo" % "0.17.1-play27"

libraryDependencies += "org.mockito" % "mockito-core" % "2.28.2" % Test
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.23" % Test

// https://mvnrepository.com/artifact/com.typesafe.play/play-json-joda
libraryDependencies += "com.typesafe.play" %% "play-json-joda" % "2.7.4"

libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.25" % Test

libraryDependencies += "com.enragedginger" %% "akka-quartz-scheduler" % "1.8.1-akka-2.5.x"

resolvers in ThisBuild += "Atlassian Releases" at "https://maven.atlassian.com/public/"
// https://mvnrepository.com/artifact/com.mohiva/play-silhouette
libraryDependencies ++= Seq(
  "com.mohiva" %% "play-silhouette" % "6.1.0",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "6.1.0",
  "com.mohiva" %% "play-silhouette-crypto-jca" % "6.1.0",
  "com.mohiva" %% "play-silhouette-persistence" % "6.1.0",
  "com.mohiva" %% "play-silhouette-testkit" % "6.1.0" % "test"
)
libraryDependencies ++= Seq(
  "com.mohiva" %% "play-silhouette-cas" % "6.1.0",
  "com.mohiva" %% "play-silhouette-totp" % "6.1.0",
  "com.mohiva" %% "play-silhouette-persistence-reactivemongo" % "5.0.6"
)
